import os

print((os.urandom(20))) # 20 random characters
list(map(ord, _))

'''
from here: http://beam.wtf/Ye4A
Sorry but...

    In [1]: import os

    In [2]: os.urandom(20) # 20 random characters
    Out[2]: 'Q\xe8\xb5\x96\xfb\xba\xab\xff\x9dF(\x80`\xb9c\xff\xb3\t\x96\xb1'

    In [3]: map(ord, _) # Full character range 0-255
    Out[3]: [81, 232, 181, 150, 251, 186, 171, 255, 157, 70, 40, 128, 96, 185, 99, 255, 179, 9, 150, 177]

    In [4]: %timeit os.urandom(1024) # 6.5ms/MB
    100000 loops, best of 3: 6.5 Âµs per loop
'''
