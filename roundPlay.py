# playing with round()

import console
from urllib.request import urlretrieve

console.clear()
title = 'Convert USD to TWD'
print(title)
print('=' * 40)
print('please enter the amount in US dollars')
myValue = input()
# maybe we could download this from somewhere when run?
# look at urllib.request in docsâ€¦ duck duck go had a good conversion tool online maybe uodate once daily!
# also here:
#	https://ashokfernandez.wordpress.com/2013/09/24/using-open-exchange-rates-with-python/
myRate = 32.385

# print(myValue+' is the number entered')


# convert to integer or float so we can do math
myValueInt = float(myValue)
value = myValueInt * myRate
# myValueRound = round(value,2)
myValueRound = round(value)
print('multiplying ' + myValue + ' by exchange rate of ' + str(myRate) + ' gives a total of ' + str(myValueRound))
