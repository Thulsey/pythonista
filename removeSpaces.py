# strips all spaces from a string
# on the clipboard or selected text (like in drafts.app)
# initially for tr and s chinese ocr
# but that problem was beig created
# by my script so it was my own fault
# and is now fixed.

import clipboard
import re
import sys
import webbrowser
import urllib

replace = 'null'
numArgs = len(sys.argv) 
if numArgs < 4:
	txt = clipboard.get() 
else: 
	replace='yes'
	txt = sys.argv[1] 
	uuid = sys.argv[2]
	selection_start = sys.argv[3]
	selection_length = sys.argv[4]
	print(txt + uuid + selection_start + selection_length)
txt = re.sub(" +", "", txt)

if replace == 'yes': # this has been passed from an app
	url = 'drafts4://replaceRange?uuid=' + uuid + '&start=' + selection_start + '&length=' + selection_length + '&text=' + urllib.parse.quote(txt)
	
	#print(url)
	webbrowser.open(url)
else: # we are running this on the clipboard
	clipboard.set(txt)
	print('copied the following to clipboard: ' + txt)
