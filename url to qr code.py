# coding: utf-8

import qrcode
import clipboard
import sys

def make(url):
	print(url)
	img = qrcode.make(url)
	img.show()

numArgs = len(sys.argv)
if numArgs == 2:
	url = sys.argv[1]
else:
	url = clipboard.get()

make(url)
