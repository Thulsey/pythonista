'''
try to get titlecase.py https://github.com/ppannuto/python-titlecase
'''

import sys
import clipboard
numArgs = len(sys.argv)

if numArgs == 2 :
	txt = sys.argv[1]
else:
	txt = clipboard.get()

txt = txt.title()	
clipboard.set(txt)
print('"' + txt + '" has been copied to the clipboard')
