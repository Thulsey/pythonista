# To call script from Drafts, use the following URL as URL Action:
# <pythonista://namesort?action=run&argv=[[draft]]>
import sys
import urllib.request, urllib.parse, urllib.error
import webbrowser


def lastname(s):
  n = s.split()
  if len(n) == 1:
    return n
  if n[-1] in ('Jr.', 'Sr.', 'I', 'II', 'III', 'IV', 'V'):
    suffix = n[-1:]
    firstnames = n[:-2]
    if n[-2][-1] == ',':
      return [n[-2][:-1]] + firstnames + suffix
    else:
      return [n[-2]] + firstnames + suffix
  else:
    firstnames = n[:-1]
    return [n[-1]] + firstnames

a = sys.argv[1].split("\n")
a.sort(key=lastname)
a = "\n".join(a)

url = "drafts4://x-callback-url/create?text=" + urllib.parse.quote(a)
#print(url)
webbrowser.open(url)
