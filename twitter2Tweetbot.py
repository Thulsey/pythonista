import sys 
import urllib.request, urllib.parse, urllib.error 
import webbrowser 
import clipboard 
import re 

numArgs = len(sys.argv) 
if numArgs < 2: 
	url = clipboard.get() 
else: 
	# text = sys.argv[0] 
	url = sys.argv[1] 
link = re.sub("(?P<domain>https://.*twitter\\.com/)(?P<user>.+)/(?P<status>(status|statuses))/(?P<id>.+)", "tweetbot://\g<user>/status/\g<id>", url) 

'''
todo
needs to work on profile urls too - no status or id in url
workflow or whatver calls this should send arfuments - open, copy, send to drafts, display in console
break into if/then/else for action

ignore non-twitter links on clipboard
'''

encoded = urllib.parse.quote(link, safe='') 

# do something with it!
# open in tweetbot
webbrowser.open(link)

# open in drafts
# drafts = 'drafts4://x-callback-url/create?text=' + link

# webbrowser.open(drafts)

# just show it in console as a clickable link
#print(link)
