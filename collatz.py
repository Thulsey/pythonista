# demo the collatz problem. 
# eventually, even or odd, using 
# this formula it will always result 
# in 1, also need to validate input

# this only works on iOS/pythonista

import console
console.clear()

print('The 3n+1 Problem. Any even number divided by 2,')
print('or any odd number multiplied by 3 plus 1,')
print('will eventually arrive at 1. No one knows why this pattern occurs…')

counter = 1

def increment(number):
    global counter
    counter = counter+1

def collatz(number): # the main function
    global value
    if number % 2 == 0: # even
        value = number // 2
        print(value)
    elif number % 2 == 1: # odd
        value = 3 * number + 1
        print(value)
    return value

def validateInput(input): # make sure its a int
    global value # used in a few places
    try:
        value = int(input)
        if value == 42:
            print('Haha. I get that joke.')
        return True
    except ValueError:
        print('Only a number as a digit, please.')
    return False

def getNumber(): # input a number 
    global counter
    print('Input any number:')
    number = input()
    userInput = validateInput(number)
    if userInput == True:
        while value != 1: # still global!
            increment(counter)
            collatz(value) # because not defined
    else:
        getNumber()

goAgain = 'Y'
while goAgain == 'Y':
    getNumber()		
    print('See? It took ' + str(counter) + ' tries but it finally got to 1.')
    print('Want to try again? Y or N?')
    counter = 1 #reset
    goAgain = str.capitalize(input())
else:
    print('OK. Thanks for playing!')
    print('End of line.')
