# force traditional chinese ocr
import http.client, urllib.request, urllib.parse, urllib.error, base64, clipboard, json, requests, datetime, appex, photos, console, dialogs
# no need, took out space in last line
# import removeSpaces

if appex.is_running_extension() is True:
    image = appex.get_image_data()
else:
    image = photos.pick_image(original=True, raw_data=True)


urlOcr = '/vision/v1.0/ocr?%s'

headers = {
    # Request headers
    'User-Agent': 'python',
    'Ocp-Apim-Subscription-Key': 'a3fd33f0a3004b4582faa2b89013defa',
    'Content-Type': 'application/octet-stream',
}

params2 = urllib.parse.urlencode({
    # Request parameters
    'language': 'zh-Hant', # force traditional chinese
    'detectOrientation': 'true',
})



print('Performing OCR...')


conn = http.client.HTTPSConnection('api.projectoxford.ai')
conn.request("POST", urlOcr % params2, image, headers)
response = conn.getresponse()
back = response.read()
conn.close()

print('OCR successfully performed.')
# data = json.loads(back)
back = back.decode('utf-8')
data = json.loads(back)

s = ''

for item in data["regions"]:
    for line in item["lines"]:
        for word in line["words"]:
            s += '' + word["text"]

print(s)
dialogs.share_text(s)
