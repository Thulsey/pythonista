import sys
import datetime
from webbrowser import open
from urllib.parse import quote

d = datetime.date.today()

next = d + datetime.timedelta(days= 5-d.weekday() if d.weekday()<5 else 12-d.weekday())

open('things:add?title=' + quote(sys.argv[1]) + '&notes=' + quote(sys.argv[2]) + '&dueDate=' + str(next))
